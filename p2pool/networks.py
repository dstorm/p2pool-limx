from p2pool.bitcoin import networks
from p2pool.util import math

# CHAIN_LENGTH = number of shares back client keeps
# REAL_CHAIN_LENGTH = maximum number of shares back client uses to compute payout
# REAL_CHAIN_LENGTH must always be <= CHAIN_LENGTH
# REAL_CHAIN_LENGTH must be changed in sync with all other clients
# changes can be done by changing one, then the other

nets = dict(

    limecoinx=math.Object(
        PARENT=networks.nets['limecoinx'],
        SHARE_PERIOD=15, # seconds
        CHAIN_LENGTH=24*60*60//15, # shares
        REAL_CHAIN_LENGTH=24*60*60//15, # shares
        TARGET_LOOKBEHIND=60, # shares  //with that the pools share diff is adjusting faster, important if huge hashing power comes to the pool
        SPREAD=6, # blocks
        IDENTIFIER='f697ca55fcce93b8'.decode('hex'),
        PREFIX='1f36ca7172ac5bb6'.decode('hex'),
        P2P_PORT=6884,
        MIN_TARGET=4,
        MAX_TARGET=2**256//2**20 - 1,
        PERSIST=False,
        WORKER_PORT=6885,
        BOOTSTRAP_ADDRS='limx.altmine.net'.split(' '),
        ANNOUNCE_CHANNEL='#p2pool-limx',
        VERSION_CHECK=lambda v: True,
    ),

)
for net_name, net in nets.iteritems():
    net.NAME = net_name
